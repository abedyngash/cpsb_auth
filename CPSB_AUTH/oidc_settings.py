from oidc_provider.lib.claims import ScopeClaims
from django.core.serializers import serialize
import json

def userinfo(claims, user):
	# claims['name'] = user.get_full_name
	claims['email'] = user.email

	json_str_response = serialize('json', [user.profile], ensure_ascii=False)
	response = json.loads(json_str_response)

	claims['profile'] = response
	claims['given_name'] = user.profile.first_name
	claims['family_name'] = user.profile.last_name
	# claims['middle_name'] = user.profile.middle_name
	# claims['gender'] = user.profile.gender
	# claims['birthdate'] = user.profile.date_of_birth

	return claims

class CustomScopeClaims(ScopeClaims):
	info_details = (
			('User Meta Data'),
			('List of details such as ID Number, Last Logout, and Access Rights'),
		)

	def scope_details(self):
		json_str_response = serialize('json', [self.user.profile])
		response = json.loads(json_str_response)
		# self.user - Django user instance.
        # self.userinfo - Dict returned by OIDC_USERINFO function.
        # self.scopes - List of scopes requested.
        # self.client - Client requesting this claims.
		responses = {
			'user_profile': response,
			'id_number': self.user.id_number,
			'last_login' : self.user.last_login,
			'last_logout' : self.user.last_logout,
			'user_type' : self.user.user_type,
		}

		return responses
