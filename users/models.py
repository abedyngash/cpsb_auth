from django.db import models
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.base_user import AbstractBaseUser
from django.shortcuts import reverse

from .managers import UserManager

USER_TYPE_CHOICES = (
	(0, 'Superuser'),
	(1, 'System Administrator'),
	(2, 'Board Chairperson'),
	(3, 'Board Vice-Chair'),
	(4, 'Board Member'),
	(5, 'Secretary/C.E.O'),
	(6, 'Director HR'),
	(7, 'Legal Officer'),
	(8, 'Principal HR'),
	(9, 'HRM & D Officer'),
	(10, 'Legal Researcher'),
	(11, 'Records Management Officer'),
)

GENDER = (
	('M', 'Male'),
	('F', 'Female'),
	('I', 'Intersex'),
)

# Create your models here.
class User(AbstractBaseUser, PermissionsMixin):
	
	user_type = models.PositiveSmallIntegerField('Select Designation', choices=USER_TYPE_CHOICES)

	id_number = models.CharField(unique=True, max_length=70)
	email = models.EmailField(unique=True)
	phone = models.CharField(max_length=13, null=True)

	date_joined = models.DateTimeField(auto_now_add=True)
	is_staff = models.BooleanField(default=False)
	is_active = models.BooleanField(default=True)

	last_logout = models.DateTimeField(null=True, blank=True)

	objects = UserManager()

	USERNAME_FIELD = 'id_number'
	REQUIRED_FIELDS = ['email', 'user_type']

	class Meta:
	    verbose_name = 'user'
	    verbose_name_plural = 'users' 

	@property
	def get_full_name(self):
		if self.profile.first_name and self.profile.middle_name and self.profile.last_name:
			return f'{self.first_name} {self.middle_name} {self.last_name}'
		else:
			return self.email

	'''Call Function to Email User'''
	def email_user(self, subject, message, from_email=None, **kwargs):
	    # Sends an email to this User.
	    pass
	    # send_mail(subject, message, from_email, [self.email], **kwargs)

	'''Call Function when no Reverse URL is provided to the User Model'''
	def get_absolute_url(self):
	    return reverse('home')

	'''Default String Return Value'''
	def __str__(self):
		return self.id_number

class Profile(models.Model):
	user = models.OneToOneField(User, on_delete=models.CASCADE)
	first_name = models.CharField(max_length=100, blank=True)
	middle_name = models.CharField(max_length=100, blank=True)
	last_name = models.CharField(max_length=100, blank=True)
	date_of_birth = models.DateField(null=True)
	gender = models.TextField(max_length=1, choices=GENDER, blank=True)
	personal_number = models.BigIntegerField(null=True, blank=True)
	date_of_appointment = models.DateField(null=True)

	@property
	def get_full_name(self):
		if self.first_name and self.middle_name and self.last_name:
			return f'{self.first_name} {self.middle_name} {self.last_name}'
		else:
			return None

	@property
	def profile_complete(self):
		if self.first_name and self.middle_name and self.last_name and self.date_of_birth and self.gender:
			return True
		return False
	

	def __str__(self):
		if self.first_name and self.middle_name and self.last_name:
			return f'{self.first_name} {self.middle_name} {self.last_name}'
		else:
			return self.user.id_number