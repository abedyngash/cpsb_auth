from django.contrib.auth.signals import user_logged_out
from django.contrib.auth.signals import user_logged_in
from django.contrib.auth.models import update_last_login
from django.db.models.signals import post_save
from django.dispatch import receiver
from .models import User, Profile
import datetime

''' SIGNAL TO CREATE USER`S PROFILE BASED ON USER TYPE'''
@receiver(post_save, sender=User)
def attach_user_to_profile(sender, instance, created, **kwargs):
	if created:
		Profile.objects.create(user=instance)

''' SAVING THE CREATED PROFILE '''
@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
	instance.profile.save()

''' SIGNAL TO UPDATE USER`S LOGOUT TIMESTAMP '''
@receiver(user_logged_out)
def sign_user_out(sender, user, request, **kwargs):
	request.user.last_logout = datetime.datetime.now()
	request.user.save()
	