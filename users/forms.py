from django.shortcuts import redirect
from django import forms
from django.contrib import messages
from django.contrib.auth import login
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm

from allauth.account.forms import LoginForm, SignupForm

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Row, Column, Field
from crispy_forms.bootstrap import PrependedText, PrependedAppendedText, AppendedText

from .models import User, Profile, GENDER

''' LOGIN FORM '''
class LoginForm(LoginForm):
	"""docstring for LoginForm"""
	def __init__(self, *args, **kwargs):
		super(LoginForm, self).__init__(*args, **kwargs)
		self.helper = FormHelper()
		self.helper.form_tag = False
		self.fields["login"].label = ""
		self.fields["password"].label = ""
		self.helper.layout = Layout(
			Field('login', placeholder="Email Address or ID Number"),
			AppendedText('password', '<i class="fas fa-eye-slash" id="eye" onclick="showHidePwd();"></i>', placeholder="Enter Password"),

			Field('remember'),
		)


''' EMPLOYEES REGISTRATION FORM '''
class RegistrationForm(UserCreationForm):
	first_name = forms.CharField(max_length=100)
	middle_name = forms.CharField(max_length=100)
	last_name = forms.CharField(max_length=100)

	date_of_birth = forms.DateField()
	gender = forms.ChoiceField(choices=GENDER)

	personal_number = forms.IntegerField()
	date_of_appointment = forms.DateField()

	


	class Meta:
		model = User
		fields = [
			'id_number', 'user_type', 'email', 'phone',
			'password1', 'password2'
		]

	def signup(self, request, user):
		login(request, user)

		messages.success(request, 'Your details have been saved Successfully')
		return redirect('home')

	def save(self, commit=False):
		user = super(RegistrationForm, self).save(commit=False)
		# user.user_type = 3
		user.save()

		user.refresh_from_db()
		user.profile.first_name = self.cleaned_data['first_name']
		user.profile.middle_name = self.cleaned_data['middle_name']
		user.profile.last_name = self.cleaned_data['last_name']

		user.profile.date_of_birth = self.cleaned_data['date_of_birth']
		user.profile.gender = self.cleaned_data['gender']

		user.profile.personal_number = self.cleaned_data['personal_number']
		user.profile.date_of_appointment = self.cleaned_data['date_of_appointment']
		
		user.save()

		return user
			

	def __init__(self, *args, **kwargs):
	    super(RegistrationForm, self).__init__(*args, **kwargs)
	    self.fields['personal_number'].required=False
	    self.helper = FormHelper()
	    self.helper.form_tag = False

	    self.helper.layout = Layout(
	    		Row(
	    			Column(
    					Field('user_type'),
	    			),
	    			Column(
    					Field('id_number'),
	    			)
	    		),
	    		Row(
					Column(
    					Field('first_name'),
	    			),
	    			Column(
    					Field('middle_name'),
	    			),
	    			Column(
    					Field('last_name'),
	    			),
				),
	    		Row(
	    			Column(
    					Field('date_of_birth'),
	    			),
	    			Column(
    					Field('gender'),
	    			),
	    		),	    		
	    		Row(
	    			Column(
    					Field('email'),
	    			),
	    			Column(
    					Field('phone'),
	    			)
	    		),
	    		Row(
	    			Column(
    					Field('personal_number'),
	    			),
	    			Column(
    					Field('date_of_appointment'),
	    			)
	    		),
	    		Row(
	    			Column(
    					AppendedText('password1', '<i class="fas fa-eye-slash" id="eye" onclick="showHidePwd();"></i>'),
	    			),
	    			Column(
    					AppendedText('password2', '<i class="fas fa-eye-slash" id="eye2" onclick="showHidePwd();"></i>'),
	    			)
	    		),
	    	)

''' FORM TO UPDATE STAFF PROFILE INFO'''
class ProfileForm(forms.ModelForm):
	class Meta:
		model = Profile
		fields = ['first_name', 'middle_name', 'last_name', 'date_of_birth', 'gender']

	def __init__(self, *args, **kwargs):
	    super(ProfileForm, self).__init__(*args, **kwargs)
	    self.helper = FormHelper()
	    self.helper.form_tag = False
	    self.helper.layout = Layout(
	    		Row(
					Column(
    					Field('first_name'),
	    			),
	    			Column(
    					Field('middle_name'),
	    			),
	    			Column(
    					Field('last_name'),
	    			),
				),
	    		Row(
	    			Column(
    					Field('date_of_birth'),
	    			),
	    			Column(
    					Field('gender'),
	    			),
	    		),
	    	)
